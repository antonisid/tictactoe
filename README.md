# Description #

It's the known Tic Tac Toe game, built with the Lumen micro framework of Laravel. After the player selects a move
by clicking on a box of the board, a request on the API is sent. The api checks the board to see if there's a
chance to win or lose and determines it's move. The board gets updated based on the Api's json response.

The most significant part of the API is the MoveController which handles the ajax request and
sends back the board updated as a response. To achieve that I wrote an algorithm which determines
the next move of the bot based on certain criteria.

### Files to check ###

api/app/http/controllers/MoveController.php
api/app/repositories/MoveRepository.php (No data source yet though, that was the initial plan)
api/app/repositories/MoveRepositoryInterface.php
api/app/providers/AppServiceProvider.php

fe/index.php
fe/classes/GameFactory.php
fe/classes/TicTacToe.php
fe/template/board.php

### API set up ###

a) A virtual host identical to the following is needed as well as the respective host:
127.0.0.1 tictactoe-api.local

<VirtualHost *:80>
DocumentRoot "c:/wamp64/www/tictactoe/api/public"
ServerName tictactoe-api.local
ErrorLog "logs/tictactoe-api-error.log"
CustomLog "logs/tictactoe-api-access.log" common
</VirtualHost>

b) Composer install (The vendor folder isn't included neither in GIT nor in the .rar file.)

### FE set up ###

a) A virtual host identical to the following is needed as well as the respective host:
127.0.0.1 tictactoe.local

<VirtualHost *:80>
DocumentRoot "c:/wamp64/www/tictactoe/fe"
ServerName tictactoe.local
ErrorLog "logs/tictactoe-fe-error.log"
CustomLog "logs/tictactoe-fe-access.log" common
</VirtualHost>

The server name tictactoe.local corresponds to the url in the Cors.php file of the API.
The cors-origin is enabled (config/cors.php) in the API only for this url: http://tictactoe.local

b) Composer install 
The classes are loaded via the composer's autoloader.