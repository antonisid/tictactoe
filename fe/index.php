<?php
require_once(__DIR__ . '/vendor/autoload.php');

use GameSrc\GameFactory;
$game = GameFactory::create();
$game->displayGame();