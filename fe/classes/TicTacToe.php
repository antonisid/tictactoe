<?php

namespace GameSrc;

class TicTacToe
{

	public $gameBoard = array();


    public function __construct()
    {
        $this->createBoard();
    }

    /**
	* Create the initial board for the game
	* Assign the array to the gameBoard property
	**/

    private function createBoard()
    {

    	for ($y = 0; $y <= 2; $y++)
        {
            for ($x = 0; $x <= 2; $x++)
            {
                $this->gameBoard[$x][$y] = '';
            }
        }
    }

    public function displayGame()
	{
		require_once('/template/board.php');
	}
}