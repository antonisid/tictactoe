<?php

/**
 * This class by using the Factory design pattern returns an instance 
   of the TicTacToe class, which generates the initial board of the game.
 */

namespace GameSrc;

class GameFactory
{
    public static function create()
    {
        return new TicTacToe();
    }
}