
<html>
	<head>
		<title>Tic Tac Toe</title>
		<link rel="stylesheet" type="text/css" href="template/style/style.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="template/js/scripts.js"></script>
	</head>
	<body>
		<div id="container">
		<?php
		foreach ($this->gameBoard as $y => $yArray) {
			foreach ($yArray as $x => $value) {
				echo '<div class="cell" x="'.$x.'" y="'.$y.'" value="'.$value.'">';
				echo '</div>';
			}
		}
		?>
		</div>
	</body>
</html>