
$(document).ready(function() {

    function checkWinner(data) {

      // Possibilities of win
      var firstRow = [data[0], data[1], data[2]];
      var secondRow = [data[3], data[4], data[5]];
      var thirdRow = [data[6], data[7], data[8]];
      var firstColumn = [data[0], data[3], data[6]];
      var secondColumn = [data[1], data[4], data[7]];
      var thirdColumn = [data[2], data[5], data[8]];
      var leftRightDiagonal = [data[0], data[4], data[8]];
      var RightLeftDiagonal = [data[2], data[4], data[6]];

      var arrayToCheck = [firstRow, secondRow, thirdRow, firstColumn, secondColumn, thirdColumn, leftRightDiagonal, RightLeftDiagonal];

      $.each(arrayToCheck, function(i, possibility) {
        if (possibility[0].unit != '') {
          if (possibility[0].unit == possibility[1].unit && possibility[1].unit == possibility[2].unit) {
            if (possibility[0].unit == 'X') {
              text = 'You';
            }
            else {
              text = 'O player';
            }
            $('#container').append('<div id="success"><p>'+text+' Won</p></div>');
            return false;
          }
        }
      });
    }

    $(document).on('click','.cell',function(event) {

    $(this).text('X');
    $(this).attr('value', 'X');

    jsonObj = [];

    $('.cell').each(function() {

        var x = $(this).attr("x");
        var y = $(this).attr("y")
        var unit = $(this).attr("value");

        item = {}
        item ['x'] = x;
        item ['y'] = y;
        item ['unit'] = unit;

        jsonObj.push(item);
    });

      var firstRow = [jsonObj[0], jsonObj[1], jsonObj[2]];
      var secondRow = [jsonObj[3], jsonObj[4], jsonObj[5]];
      var thirdRow = [jsonObj[6], jsonObj[7], jsonObj[8]];
      var firstColumn = [jsonObj[0], jsonObj[3], jsonObj[6]];
      var secondColumn = [jsonObj[1], jsonObj[4], jsonObj[7]];
      var thirdColumn = [jsonObj[2], jsonObj[5], jsonObj[8]];
      var leftRightDiagonal = [jsonObj[0], jsonObj[4], jsonObj[8]];
      var RightLeftDiagonal = [jsonObj[2], jsonObj[4], jsonObj[6]];

      var arrayToCheck = [firstRow, secondRow, thirdRow, firstColumn, secondColumn, thirdColumn, leftRightDiagonal, RightLeftDiagonal];
      found = false;
      $.each(arrayToCheck, function(i, possibility) {
        if (possibility[0].unit != '') {
          if (possibility[0].unit == possibility[1].unit && possibility[1].unit == possibility[2].unit) {
            if (possibility[0].unit == 'X') {
              text = 'You';
              found = true;
            }
            else {
              text = 'O player';
              found = true;
            }
            $('#container').append('<div id="success"><p>'+text+' Won</p></div>');
          }
        }
      });

    if (found == false) {
      $.ajax({
        url: 'http://tictactoe-api.local/api/v1/move',
        type: 'POST',
        crossDomain: true,
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        cache: false,
        success: function(data, status) {
          $('#container').html('');
          var board = '';
          for(var i in data) {
           board += '<div class="cell" x="'+data[i].x+'" y="'+data[i].y+'" value="'+data[i].unit+'">'+data[i].unit+'</div>';
          }
          $('#container').html(board);
          checkWinner(data);
        },
        error: function(xhr, desc, err) {
          console.log(xhr);
          console.log("Details: " + desc + "\nError:" + err);
        }
      });
    }
  });
});