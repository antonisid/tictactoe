<?php
namespace App\Repositories;
 
interface MoveRepositoryInterface {

    
    public function move($board);

    public function availableSlots($board);

    public function randomSpot();

    public function updateBoard($nextMove);

    public function possibleWins();

    public function approachToMove($possibleWins);

    public function countSameUnits($possibleWins);

    public function saveTheGame($rowColumn);

    public function winTheGame($rowColumn);
        
}