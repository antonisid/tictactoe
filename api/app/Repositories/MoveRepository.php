<?php

/**
 * The algorithm for the moves of the bot based on the current board which was sent through Ajax
 */

namespace App\Repositories;
  
class MoveRepository implements MoveRepositoryInterface {

	protected $difficultyLevel = 'high';
	public $arrayAvailableSlots = array();
	private $arrayBoard = array();

	/**
	 * Method that updates the board with the move of the bot
	   a) Fills the $arrayBoard property with the current board
	   b) Takes into account all the possible combinations of winning the game
	   c) Determines the approach to move based on the difficulty level and if there's a
	      chance to win or lose the game
	   d) Update and return the board
	 */
	
	public function move($arrayBoard)
	{
		$this->arrayBoard = $arrayBoard;
		$possibleWins = $this->possibleWins();
		$nextMove = $this->approachToMove($possibleWins);
		$this->updateBoard($nextMove);
	    return $this->arrayBoard;
	}

	/**
	 * The available slots in the board, having filled beforehand from the MoveController
	 */

	public function availableSlots($arrayBoard)
	{
		foreach ($arrayBoard as $key => $cell) {
			
			if (empty($cell['unit'])) 
			{
				array_push($this->arrayAvailableSlots, $key);
			}
		}
	}

	/**
	 * If the difficulty of the game is not set as high it returns a random cell of the board,
	   as the next move. Otherwise it first checks if the bot can win the game and picks the "wining" cell
	   for the next move. If there's no chance to win it checks if it's about to lose the game. 
	   If both cases are not valid it picks a ranodm cell.
	 */

	public function approachToMove($possibleWins)
	{
		if ($this->difficultyLevel !== 'high') {
			return $this->randomSpot();
		}
		$loseWin = $this->countSameUnits($possibleWins);

		if (!empty($loseWin))
		{
			if(array_key_exists('win', $loseWin))
			{
				return $this->winTheGame($loseWin['win']);
			}
			else
			{
				return $this->saveTheGame($loseWin['save']);
			}
		}
		else
		{
			return $this->randomSpot();
		}
	}

	/**
	 * Check with the array_count_values() if two cells in a row are occupied 
	   from the same unit (X,O). If so the respective row or column is returned (see possibleWins()).
	   Examples: [O, O, ''] => [O, O, O], [X, X, ''] => [X, X, O]
	   Afterwards the saveTheGame() or winTheGame() return the key of the empty cell.
	   If the scenario above isn't valid a random cell is picked for the next move
	 */

	public function countSameUnits($possibleWins)
	{
		$loseWin = array();
		foreach ($possibleWins as $key => $rowColumn) {
			
			$countSameUnits = array_count_values($rowColumn);
			if (isset($countSameUnits['O']) && $countSameUnits['O'] == 2 && array_search('', $rowColumn) !== FALSE)
			{
				$loseWin['win'] = $rowColumn;
			}

			if (isset($countSameUnits['X']) && $countSameUnits['X'] == 2 && array_search('', $rowColumn) !== FALSE)
			{
				$loseWin['save'] = $rowColumn;
			}
		}
		return $loseWin;
	}

	/**
	 * Returns the key of the empty cell which is the same with the key of the arrayBoard
	 */

	public function saveTheGame($rowColumn)
	{

		return array_search('', $rowColumn);
	}

	/**
	 * Returns the key of the empty cell which is the same with the key of the arrayBoard
	 */

	public function winTheGame($rowColumn)
	{

		return array_search('', $rowColumn);
	}

	public function randomSpot()
	{
		$randomKey = array_rand($this->arrayAvailableSlots);
		return $this->arrayAvailableSlots[$randomKey];
	}

	/**
	 * Update the board before with the selected cell for the next move
	 */

	public function updateBoard($nextMove)
	{
		$this->arrayBoard[$nextMove]['unit'] = 'O';
	}

	/**
	 * Convert the array of the board in an array which contains all the possible ways of winning
	   the game. Every element of the array it has as a key the actual key of the "original" board,
	   in order to be known at the saveTheGame() & winTheGame() methods.
	 */
	public function possibleWins()
	{

	  $firstRow = [0 => $this->arrayBoard[0]['unit'], 1 => $this->arrayBoard[1]['unit'], 
	  2 => $this->arrayBoard[2]['unit']];

      $secondRow = [3 => $this->arrayBoard[3]['unit'], 4 => $this->arrayBoard[4]['unit'], 
      5 => $this->arrayBoard[5]['unit']];

      $thirdRow = [6 => $this->arrayBoard[6]['unit'], 
      7 => $this->arrayBoard[7]['unit'], 8 => $this->arrayBoard[8]['unit']];

      $firstColumn = [0 => $this->arrayBoard[0]['unit'], 
      3 => $this->arrayBoard[3]['unit'], 6 => $this->arrayBoard[6]['unit']];

      $secondColumn = [1 => $this->arrayBoard[1]['unit'], 
      4 => $this->arrayBoard[4]['unit'], 7 => $this->arrayBoard[7]['unit']];

      $thirdColumn = [2 => $this->arrayBoard[2]['unit'], 
      5 => $this->arrayBoard[5]['unit'], 8 => $this->arrayBoard[8]['unit']];

      $leftRightDiagonal = [0 => $this->arrayBoard[0]['unit'], 
      4 => $this->arrayBoard[4]['unit'], 8 => $this->arrayBoard[8]['unit']];

      $RightLeftDiagonal = [2 => $this->arrayBoard[2]['unit'], 
      4 => $this->arrayBoard[4]['unit'], 6 => $this->arrayBoard[6]['unit']];

      $arrayToCheck = [$firstRow, $secondRow, $thirdRow, $firstColumn, 
      				   $secondColumn, $thirdColumn, $leftRightDiagonal, $RightLeftDiagonal];
      return $arrayToCheck;
	}

}