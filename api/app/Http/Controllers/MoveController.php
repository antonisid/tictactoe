<?php

/**
 * The controller that determines the moves of the bot through this api.
 * It uses an interface to de-couple the controller from the responsible for the next move 
 * class, in case another algorithm needs to be added for the next move.The interface is available
   to the application by using the dependency injection design pattern.
 */

namespace App\Http\Controllers;
use App\Repositories\MoveRepositoryInterface;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


class MoveController extends Controller
{
    
    protected $algorithm;
    private $arrayBoard = array();

    public function __construct(MoveRepositoryInterface $algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * Based on the Json request this method checks if the board is already full. If so
     * it returns the board as it is. Otherwise it sends back the board updated with the 
     * move of the bot.
     */

    public function nextMove(Request $request)
    {   
        
        $this->handleRequest($request);

        if (!empty($this->arrayBoard))
        {   if ($this->fullBoard($this->arrayBoard))
            {
                $board = $this->arrayBoard;
            }
            else {
                $board = $this->algorithm->move($this->arrayBoard);
            }
        }

        return response()->json($board);
    }

    /**
     * Decode the json request
     */

    private function handleRequest($request)
    {
        if ($request->isJson()) {

            $this->arrayBoard = json_decode($request->getContent(), true);
        }
    }

    /**
     * Check if the board is full
     */

    private function fullBoard($board)
    {

        $this->algorithm->availableSlots($board);
        if (empty($this->algorithm->arrayAvailableSlots))
        {
            return true;
        }
        return false;
    }

}
