<?php

namespace App\Providers;

use App\Repositories\MoveRepository;
use App\Repositories\MoveRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  
    public function register()
    {

        $this->app->bind('App\Repositories\MoveRepositoryInterface', 'App\Repositories\MoveRepository');
    }
}
