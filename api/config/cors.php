<?php

return [
     
    'supportsCredentials' => false,
    'allowedOrigins' => ['http://tictactoe.local'],
    'allowedHeaders' => ['Content-Type', 'application/json; charset=utf-8'],
    'allowedMethods' => ['POST'],
    'exposedHeaders' => [],
    'maxAge' => 0,
];
